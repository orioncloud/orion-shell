# orion-shell

#### 介绍
猎户座一键部署脚本，打造最快捷安装服务器计划。

#### 安装教程

1.  推荐从本仓库的发行版直接下载shell脚本
    
    ``` wget https://gitee.com/orioncloud/orion-shell/releases/download/v1.0.0/install_all.sh ```

2. 将脚本放到服务器的目录下，推荐 /home 目录
3. 执行 sudo sh install_all.sh
4. ![img.png](img.png)
5. 输入对应的序号即可开始安装。


#### 注意
1. java8由于下载包限制，所以需要手动上传。上传到 install_all.sh 脚本同级目录下即可。
2. jenkins 版本为2.346.1 最后支持java8的版本。有需要可以自行更换。https://get.jenkins.io/war-stable/
3. nodejs 安装版本为x64系统。
4. 所有脚本只针对linux，暂不支持 windows 和 macos